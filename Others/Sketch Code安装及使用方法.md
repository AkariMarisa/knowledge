# Sketch Code使用教程

Sketch Code是一个通过深度学习，可以将手写界面原型图片转换为可以使用HTML代码的程序。

## 1. 安装

### 依赖环境

* Python 3

* pip

### 安装

    # 下载源文件， 约1700张图片，共计342mb
    git clone https://github.com/ashnkumar/sketch-code.git
    cd sketch-code
    cd scripts

    # 获取训练好的模型与数据
    sh get_data.sh
    sh get_pretrained_model.sh

如果需要，可以安装依赖环境

    pip install -r requirements.txt

## 2. 使用

### 示例

    cd src

    python convert_single_image.py --png_path ../examples/drawn_example1.png \
        --output_folder ./generated_html \
        --model_json_file ../bin/model_json.json \
        --model_weights_file ../bin/weights.h5

### 将单张图片转换为HTML代码

    cd src

    python convert_single_image.py --png_path 原型图片.png \
        --output_folder HTML的输出目录 \
        --model_json_file 训练模型的JSON文件 \
        --model_weights_file 训练模型的关系文件

### 将多张图片转换为HTML代码并放到一个目录

    cd src

    python convert_batch_of_images.py --pngs_path 多张原型图片的目录 \
        --output_folder HTML的输出目录 \
        --model_json_file 训练模型的JSON文件 \
        --model_weights_file 训练模型的关系文件

### 训练模型

    cd src

    # 使用数据训练程序
    # <augment_training_data> 使用Keras ImageDataGenerator参数来训练图片
    python train.py --data_input_path 用来训练的界面图片路径 \
        --validation_split 0.2 \
        --epochs 10 \
        --model_output_path 训练好的模型的输出路径
        --augment_training_data 1

    # 使用已经训练好的模型进行训练
    python train.py --data_input_path 用来训练的界面图片路径 \
        --validation_split 0.2 \
        --epochs 10 \
        --model_output_path 训练好的模型的输出路径 \
        --model_json_file ../bin/model_json.json \
        --model_weights_file ../bin/pretrained_weights.h5 \
        --augment_training_data 1

### 使用BLEU分数评估生成的预测

    cd src

    # 评估单个界面的训练结果
    python evaluate_single_gui.py --original_gui_filepath  原界面文件的路径 \
        --predicted_gui_filepath 训练过得界面图片路径

    # 评估多个界面的训练结果
    python evaluate_batch_guis.py --original_guis_filepath  原界面文件的路径 \
        --predicted_guis_filepath 训练过得界面图片路径
