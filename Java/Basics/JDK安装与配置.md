# JDK与JRE的安装与配置

## Windows

太简单了，这里就不说了，环境变量配置参考Linux

## Linux

1. 下载tar.gz的压缩包，并解压到任意目录下。

2. 修改/etc/profile文件，将以下内容添加进去
>JAVA_HOME=/path/to/your/jdk  
>CLASS_PATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar  
>PATH=.:$JAVA_HOME/bin:$PATH  
>export JAVA_HOME  
>export CLASS_PATH  
>export PATH  

3.执行以下指令，使当前环境变量生效
>source /etc/profile