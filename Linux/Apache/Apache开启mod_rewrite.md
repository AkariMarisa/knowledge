# Apache开启mod_rewrite

## 第一步：检查模块是否存在

在启动 mod_rewrite 模块之前，我们需要去检查一下当前模块是否已被加载到 Apache 中。

执行以下指令来进行检查。

```
# httpd -V
```

执行完指令之后，输出的信息应该和下面相似。

```
Server version: Apache/2.2.15 (Unix)
Server built:   Jan 12 2017 17:09:39
Server's Module Magic Number: 20051115:25
Server loaded:  APR 1.3.9, APR-Util 1.3.9
Compiled using: APR 1.3.9, APR-Util 1.3.9
Architecture:   64-bit
Server MPM:     Prefork
threaded:     no
forked:     yes (variable process count)
Server compiled with....
-D APACHE_MPM_DIR="server/mpm/prefork"
-D APR_HAS_SENDFILE
-D APR_HAS_MMAP
-D APR_HAVE_IPV6 (IPv4-mapped addresses enabled)
-D APR_USE_SYSVSEM_SERIALIZE
-D APR_USE_PTHREAD_SERIALIZE
-D APR_HAS_OTHER_CHILD
-D AP_HAVE_RELIABLE_PIPED_LOGS
-D DYNAMIC_MODULE_LIMIT=128
-D HTTPD_ROOT="/etc/httpd"
-D SUEXEC_BIN="/usr/sbin/suexec"
-D DEFAULT_PIDLOG="run/httpd.pid"
-D DEFAULT_SCOREBOARD="logs/apache_runtime_status"
-D DEFAULT_LOCKFILE="logs/accept.lock"
-D DEFAULT_ERRORLOG="logs/error_log"
-D AP_TYPES_CONFIG_FILE="conf/mime.types"
-D SERVER_CONFIG_FILE="conf/httpd.conf"
```

根据上面的结果，我们可以看到 Apache 的配置文件路径（httpd.conf）。从结果中得知，Apache 的配置文件在 /etc/httpd/conf/httpd.conf 这个位置。

运行以下指令，检查模块是否存在。

```
# ls /etc/httpd/modules | grep mod_rewrite
```

如果模块存在的话，那么你应该会得到下面的输出。

```
mod_rewrite.so
```

然而还是有极小概率的可能会出现模块不存在的情况，此时你应该安装 mod_rewrite 然后重新编译 Apache 对模块的支持。

## 第二步：检查模块是否已经加载

现在我们确认了模块是存在的了，接下来我们来检查模块是否已经被启用了。通过运行以下的指令来进行检查。

```
# grep -i LoadModule /etc/httpd/conf/httpd.conf | grep rewrite
```

如果你能得到以下的输出结果，那么就意味着模块已经被启用了。

```
LoadModule rewrite_module modules/mod_rewrite.so
```

如果你能得到下面的输出结果，那么把这一行前的注释（#）去掉，然后保存配置。

```
#LoadModule rewrite_module modules/mod_rewrite.so
```

但是如果你什么结果都得不到，那么就把下面这行内容加到 httpd.conf 配置文件中。

```
LoadModule rewrite_module modules/mod_rewrite.so
```

## 第三歩：配置httpd.conf文件

现在我们启用了 mod_rewrite 模块，接下来我们需要配置 .htaccess 文件通过 mod_rewrite 来实现 URL 重写，你需要在 Apache 全局配置针对每个 directory 节点启用重写功能。

运行下面的指令检查是否开启了重写功能。

```
# grep -i AllowOverride /etc/httpd/conf/httpd.conf
```

输出样例：

```
AllowOverride None
# AllowOverride controls what directives may be placed in .htaccess files.
    AllowOverride None
#    AllowOverride FileInfo AuthConfig Limit
# for additional configuration directives.  See also the AllowOverride
    AllowOverride None
    AllowOverride None
        AllowOverride None
```

修改 httpd.conf 配置文件，将你需要修改的 directory 节点对应的 AllowOverride 选项从 None 修改到 All 即可。修改完成后保存配置文件。

就这样你成功启用了 mod_rewrite 模块，enjoy～