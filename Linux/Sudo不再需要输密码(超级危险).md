# 如何让sudo不再需要密码

## 警告

首先确认你知道这么做的后果，以及为以后可能会出现的问题承担责任，并且你是在清醒状态进行的这些操作。如果没有问题，那么你可以继续。

## 步骤

备份你的系统下 /etc/sudoers 文件，方便后期回退。

执行以下指令，进行编辑 /etc/sudoers 文件，

```
$ sudo visudo
```

在文件中查找需要免密sudo的用户名，这里以tom为例。如果不存在，则新增一行。修改内容如下

> tom ALL=(ALL) NOPASSWD:ALL

这样做范围实在太大，所有sudo命令都将不再需要密码。这样一来 sudo rm -rf / 岂不就是大杀器。所以有心的用户可以单独设定那些指令不需要密码，具体内容如下

> tom ALL = NOPASSWD: /bin/kill /bin/systemctl

NOPASSWD后面跟着的是需要免密的指令，这样一来可以有效的控制权限，免得出现像 sudo rm -rf / 这样SB的问题。

保存并退出 /etc/sudoers 文件的编辑状态。

Enjoy