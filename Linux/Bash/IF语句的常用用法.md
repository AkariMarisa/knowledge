# 日常IF

## IF判断文件(目录)是否存在

    ```
    #！/bin/sh

    if [ ! -d "/path/to/myfolder" ]; then
        mkdir /path/to/myfolder
    fi

    if [[ ! -d "/path/to/myfolder/something.txt" ]]; then
        touch /path/to/myfolder/something.txt
    fi
    ```

## IF判断环境变量是否存在

    ```
    #!/bin/sh

    if [ $ORACLE_HOME ];then
        echo "ORACLE_HOME = $ORACLE_HOME"
    else
        echo "ORACLE IS NOT EXISTS"
    fi
    
    if [ -z $JAVA_HOME ];then
        echo "not exists"
    else
        echo "JAVA_HOME = $JAVA_HOME"
    fi
    ```

## 未完待续...