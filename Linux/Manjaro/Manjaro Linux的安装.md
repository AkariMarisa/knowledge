# 前言

## 介绍

Manjaro Linux是一款基于ArchLinux的再发行版，使用Arch系的pacman作为包管理器，同时又提供pamac、yay和octopi三种前端软件来简化pacman的操作。因为使用的是ArchLinux的软件仓库，所以软件更新速度非常快。同时官方提供了stable、testing、unstable三条软件更新通道，进一步控制Arch系软件更新过快导致的各种问题，可谓在Arch的基础上进一步的提升了稳定性。而Manjaro同时又提供了AUR（Arch User Repository）仓库供用户使用，在软件数量方面处于绝对优势，大部分出现在Linux上的软件都可以找到。但同时因为时用户自己维护的软件仓库，安全性方面不如官方仓库来的安全，所以在安装软件方面还需要用户多加考虑。

因为国内Deepin Linux的兴起，导致一些优质的软件也涌入的ArchLinux的仓库，从而让Arch系的用户能用上一些通过Wine运行的软件，其中就包括QQ、微信。如果之前这两个软件是大家对Linux敬而远之的理由，那么我相信现在会让一些用户再考虑一番。

## 为什么选择Manjaro？

其实在工作的时候就接触过很长时间的Linux/UNIX系统，不过主要还是以RedHat系为主，连鼎鼎大名的Debian系都是在业余时间才拿来玩的。最初本人还是在Win下的虚拟机来尝试当时各种Linux的发行版，什么elementary OS、MX Linux、Solus OS、Fedora Linux，还有非常有名气的Debian和Ubuntu都有玩过，但都没有一款感觉适合自己使用的。

我的使用理念是：软件必须要多，同时配置必须简单，不能像Debian系那样，配置个JDK都要麻烦死；同时至少要有Linux的样子，有一定的可玩性。像Deepin Linux、elementary OS这种的就直接不考虑了，Ubuntu和Debian用过一段时间，Ubuntu上经常出一些莫名其妙的问题，而Debian上就不会有，这个和Canonical有关系，有兴趣的可以上网上搜搜这个公司都干过啥好事。像RedHat系的因为软件版本太老，或者配置太奇怪也不在考虑范围中。

在使用Debian一段时间后，我便对apt这种包管理感觉不满，倒不是包管理器不好用，而是我使用过程中，感觉到很多强行复杂化的操作。Debian系都有的一个大毛病，就是PPA，按理说这种东西都不应该存在。因为PPA的出现，导致软件仓库碎片化，我不去网上找我都不知道这个仓库。像这种问题让我不再想长期使用Debian系的任何发行版了，而与此同时我再网上找到了Manjaro Linux。

目前这个系统已经是我工作的主力系统了，如果不是还要玩游戏的原因，估计的我家里的那台Win10也让我装成Linux了。之前用KDE桌面用了1年多，一直更新没出现过大问题。后来又用过Gnome、Xfce、Budgie这些GTK桌面，除了Budgie滚挂过一次，而且还是systemd更新的问题，其他使用起来都很舒服。如果不出大问题，Manjaro将会一直用下去。

# TL;DR的正文

## 下载

在[Manjaro的官网](https://manjaro.org/download/)可以下载ISO的镜像，其中有一些是开发版的系统，因为可能存在问题，这里不推荐使用。

下载下来注意校验下SHA1值，有些时候如果下载下来的镜像有问题，安装的时候会一直卡住装不上。这里推荐一个Win10下可以用的指令。

> certutil -hashfile targetFile SHA1

## 制作LiveCD

Windows下推荐使用[Etcher](https://www.balena.io/etcher/)这个工具，比网上传的什么rufus什么的好用多了，基本不会出问题。使用也很简单，选择好ISO文件，选择好USB设备，点击Flash就刷入了。

Linux下的话，CLI里可以用dd，图形化里可以用Brasero，选择还是比较多的。

这是[官网的Wiki](https://wiki.manjaro.org/index.php?title=Burn_an_ISO_File)，英语好的可以参考一下。

(啥？Mac？Mac是啥？不懂，没用过)

## 安装

U盘插电脑，BIOS里设置U盘为第一启动项，还有注意是不是UEFI启动方式，这里就不详细说明。

启动到U盘的第一个界面是这样的

![2019-02-21_17-28.png](https://i.loli.net/2019/02/21/5c6e7d10ab969.png)

这里注意，界面上像tz、keytable、lang不需要修改，除非你的键盘布局不是常规的QWERTY，那么需要修改一下keytable，其他的都不需要动，选择Boot这一项回车即可。

等待开机完成后，就进入了系统默认桌面。

![2019-02-21_17-55.png](https://i.loli.net/2019/02/21/5c6e7df71a840.png)

这里使用的是Xfce桌面环境，像其他Gnome、KDE会有一些差异，但是基本上是一样的。

关掉这个默认弹出的帮助界面，首先要做的不是打开安装程序，而是修改当前pacman的镜像地区。

打开一个终端，执行以下操作。

> sudo pacman-mirrors -c China

LiveCD的默认密码是**manjaro**。

等待镜像地区更新到中国之后，将pacman的缓存刷新一下。

> sudo pacman -Sy

完成操作后，准备开始安装了。打开桌面上的安装引导程序。

![2019-02-21_18-18.png](https://i.loli.net/2019/02/21/5c6e7f816cd0f.png)

安装程序打开后，首先会让你选择安装语言，推荐使用英语，如果不熟悉英语的话，最好还是换成中文，如图。

![2019-02-21_18-18_1.png](https://i.loli.net/2019/02/21/5c6e7fc0d9e29.png)

安装流程很简单，基本上没有什么需要说的，按照提示下一步就行。

到分区这里，新手用自动分区就可以了，一劳永逸。

![2019-02-21_18-20.png](https://i.loli.net/2019/02/21/5c6e801074f6d.png)

新版Manjaro在这一步新增了一个swap分区选项，可以让用户选择是否创建swap分区，以及设置是否使用swap分区进行休眠。如果电脑内存很大，并且不需要休眠功能的话，可以不创建swap分区。

![2019-02-21_18-20_1.png](https://i.loli.net/2019/02/21/5c6e80da60115.png)

blahblahblah一番，继续下一步。设置好用户名密码，无脑下一步。

![2019-02-21_18-21.png](https://i.loli.net/2019/02/21/5c6e80f13438b.png)

最后一步，需要确认了，如果没有问题，下一步即可。

![2019-02-21_18-21_1.png](https://i.loli.net/2019/02/21/5c6e811668f24.png)

开始安装咯，泡杯茶等一下。

![2019-02-21_18-21_2.png](https://i.loli.net/2019/02/21/5c6e81380bcc5.png)

等待安装完成之后，可以选择立即重启，也可以关掉安装程序，继续使用安装镜像进行其他操作。

![2019-02-21_18-21_2.png](https://i.loli.net/2019/02/21/5c6e81380bcc5.png)

总之，安装流程到这一步就算结束了。