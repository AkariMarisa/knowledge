# Anarchy Linux

## 一、 更新安装源

安装镜像提供了 fetchmirrors 这个命令，可以用来更新本地的镜像源。使用方法如下。

```
# fetchmirrors -h
Updates pacman mirrorlist directly from archlinux.org # 从 Archlinux 官网直接更新 pacman 镜像

Usage: fetchmirrors <opts> <args> Ex: fetchmirror -q -s 4 -c US

Options:
    -c --country
    Specify your country code(s): fetchmirrors -c US AU # 下载指定国家代号的镜像
    -d --nocolor
    Disable color prompts # 关闭颜色提示
    -h --help
    Display this help message # 显示帮助信息
    -l --list
    Display list of country codes # 显示国家代号列表
    -q --noconfirm
    Disable confirmation messages # 关闭确认提示
    -s --servers
    Number of servers to rank (default 6) # 使用排名前多少的镜像源
    -v --verbose
    Verbose output # 显示消息输出

Use fetchmirrors without any option to prompt for country code(s) # 直接执行 fetchmirrors 命令并且不带任何选项，将提醒输入国家代号
```

## 二、 配置LVM (可选)

### 0. 安装 lvm 工具包

anarchy linux 的安装镜像默认没有安装 lvm 工具，需要通过 pacman 安装。

```
# pacman -Sy && pacman -S lvm2
```

### 1. 针对物理设备进行分区

如何分区这里就不赘述了，不过需要注意的是，需要将纳入 lvm 管理的分区的分区类型设置为 Linux LVM 。

### 2. 创建物理卷

首先需要检查一下当前可以使用的设备。

```
# lvmdiskscan
```

创建一个物理卷。

```
# pvcreate /path/to/your/device
```

可以通过下面命令确认设备是否已经加入逻辑卷。

```
# pvdisplay
# pvscan
```

### 3. 创建卷组

有了物理卷之后，接下来就可以创建卷组了。

首先你需要为卷组添加一个物理卷。

```
# vgcreate <your_volume_group_name> <physical_volume> # eg. vgcreate VolGroupRoot /dev/sda1
```

你可以为某一个卷组扩展物理卷。

```
# vgextend <your_volume_group_name> <physical_volume> # eg. vgextend VolGroupRoot /dev/sda2
```

添加完成后，可以通过一下命令查看卷组信息。

```
# vgdisplay
```

如果你希望在一开始就将所有物理卷添加到一个组中，你可以这样。

```
# vgcreate <your_volume_group_name> <volume1> <volume2> ... # eg. vgcreate VolGroupRoot /dev/sda1 /dev/sda2
```

### 4. 创建逻辑卷

有了物理卷，有了卷组，现在可以开始配置逻辑卷了。

```
# lvcreate -L <size> <volume_group> -n <logical_volume>
```

你可以使用卷组中的一部分空间来创建一个逻辑卷。

```
# lvcreate -L 10G VolGroupRoot -n lvolroot
```

当然，你也可以使用卷组的全部空间来填满这个逻辑卷。

```
# lvcreate -L 100%FREE VolGroupRoot -n lvolroot
```

当你完成操作后，可以查看一下当前物理卷信息。

```
# lvdisplay
```

注意，虽然通过 lvm 来管理存储空间非常方便，但是在使用过程中还是不建议将 /boot 纳入到 lvm 管理中。主要原因是如果系统出现问题，启动时 lvm 模块无法加载，则导致 /boot 分区无法被识别，此时进行系统修复时会非常麻烦。而且在一般情况中， /boot 分区的大小一般不会大变，所以在实际使用场景中，不推荐使用 lvm 管理 /boot 分区。

### 5. 创建文件系统

为逻辑卷创建文件系统和为普通存储设备创建一样的方便。

```
# mkfs.<fstype> /dev/<volume_group>/<logical_volume>
```

## 三、 启动 anarchy linux 安装器

废话不多说，执行以下命令开始安装。

```
# anarchy
```

安装器启动完成后，首先提示用户选择安装器语言，先选择 English 。

然后提示配置你的键盘布局，一般使用 us 即可。

安装器提醒是否开始安装流程，选择 Yes 。

更新镜像源，因为之前使用过 fetchmirrors 工具更新过，这里就不再操作了，跳过即可。

这一步到了选择安装系统的语言，和第一步的语言不一样，这一步选择的语言会决定你系统安装后使用的语言，这里选择最后一项 Other ，然后在下一个界面中选择 zh_CN.UTF-8。系统完全安装完成后，我们还会设置上其他的备用语言。

选择时区，这里按照你所在地方设置即可。

到了分区这一步，如果你使用了 lvm ，那么这里选择 manual partition drive 即可。如果你不希望使用 lvm 或其他情况，那么自动分区可能会适合你。这里根据情况选择即可。手动分区这一步就不再赘述了，不懂的以后可能再出一版介绍分区的文章。

分区完成之后，会跳转到主菜单，这里可以对之前的操作进行修改。如不需要修改，选择 Install Base System ，开始安装系统。

安装系统的步骤不赘述了，人家都做安装器了，流程控制的挺好的，提示也很人性化。(其实就是我懒...)。

