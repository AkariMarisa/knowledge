添加环境变量后再执行VBox即可解决。
```
env XLIB_SKIP_ARGB_VISUALS=1
```

### 2019-10-13 更新

manjaro gnome 18.1.0 Juhraya 下，命令没有dio用。直接当作环境变量配置到 /etc/enviroment 到就好了。如果不想影响到其他程序，那就把export语句放到快捷方式中，这样每次执行vbox就会绑定一次。