# 在Archlinux中启用libvirt

## 简介

> Libvirt 是一组软件的汇集，提供了管理虚拟机和其它虚拟化功能（如：存储和网络接口等）的便利途径。这些软件包括：一个长期稳定的 C 语言 API、一个守护进程（libvirtd）和一个命令行工具（virsh）。Libvirt 的主要目标是提供一个单一途径以管理多种不同虚拟化方案以及虚拟化主机，包括：KVM/QEMU，Xen，LXC，OpenVZ 或 VirtualBox hypervisors 。 
> Libvirt 的一些主要功能如下：
> * VM management（虚拟机管理）：各种虚拟机生命周期的操作，如：启动、停止、暂停、保存、恢复和迁移等；多种不同类型设备的热插拔操作，包括磁盘、网络接口、内存、CPU等。
> * Remote machine support（支持远程连接）：Libvirt 的所有功能都可以在运行着 libvirt 守护进程的机器上执行，包括远程机器。通过最简便且无需额外配置的 SSH 协议，远程连接可支持多种网络连接方式。
> * Storage management（存储管理）：任何运行 libvirt 守护进程的主机都可以用于管理多种类型的存储：创建多种类型的文件镜像（qcow2，vmdk，raw，...），挂载 NFS 共享，枚举现有 LVM 卷组，创建新的 LVM 卷组和逻辑卷，对裸磁盘设备分区，挂载 iSCSI 共享，以及更多......
> * Network interface management（网络接口管理）：任何运行 libvirt 守护进程的主机都可以用于管理物理的和逻辑的网络接口，枚举现有接口，配置（和创建）接口、桥接、VLAN、端口绑定。
> * Virtual NAT and Route based networking（虚拟 NAT 和基于路由的网络）：任何运行 libvirt 守护进程的主机都可以管理和创建虚拟网络。Libvirt 虚拟网络使用防火墙规则实现一个路由器，为虚拟机提供到主机网络的透明访问。

## 步骤1：安装KVM软件包

首先需要安装运行KVM的一些软件包：

```
$ sudo pacman -S libvirt qemu virt-manager virt-viewer dnsmasq ebtables vde2 bridge-utils openbsd-netcat
```

## 步骤2：启动KVM libvirt服务

当依赖包都安装完成后，就可以启动libvirt的服务了：

```
$ sudo systemctl enable libvirtd.service
$ sudo systemctl start libvirtd.service
```

## 步骤3：让普通用户能直接使用KVM

当我们想用普通用户来管理KVM的时候，我们需要先将用户加入到libvirt组中。
打开 /etc/libvirt/libvirtd.conf 配置文件进行修改。

```
sudo vim /etc/libvirt/libvirtd.conf
```

将 unix_sock_group 一项设置为 libvirt：

```
unix_sock_group = "libvirt"
```

将 unix_sock_rw_perms 一项设置为0700：

```
unix_sock_rw_perms = "0770"
```

然后将你的用户添加到libvirt组中。

```
sudo usermod -a -G libvirt $(whoami)
newgrp libvirt
```

重启 libvirt 守护进程。

```
sudo systemctl restart libvirtd.service
```

## 步骤4：在你的系统中使用KVM

到这一步 libvirt的安装基本上结束了，现在你可以使用 virt-manager 来创建、管理你的虚拟机了。