# Vue.js项目部署Apache

## 背景

初学Vue.js，通过 npm build 打好包之后，上传到Apache服务器上。正常访问首页没有问题，但是访问其他url的时候会报404。

## 解决方法

首先需要开启项目里vue-router的history模式，这里借用官网的教程

> vue-router 默认 hash 模式 —— 使用 URL 的 hash 来模拟一个完整的 URL，于是当 URL 改变时，页面不会重新加载。
> 如果不想要很丑的 hash，我们可以用路由的 history 模式，这种模式充分利用 history.pushState API 来完成 URL 跳转而无须重新加载页面。
> ```
> const router = new VueRouter({
>   mode: 'history',
>   routes: [...]
> })
> ```

OK，到这里项目修改就到这里，然后我们需要修改Apache服务器上的配置。这个步骤可以看我写的另一篇文章![Apache开启mod_rewrite](../../Linux/Apache/Apache开启mod_rewrite.md)

Apache 配置修改完成之后，就需要修改 .htaccess 配置了，按照下面的配置，修改 .htaccess 配置文件。

```
<IfModule mod_rewrite.c>
  RewriteEngine On
  RewriteBase /
  RewriteRule ^index\.html$ - [L]
  RewriteCond %{REQUEST_FILENAME} !-f
  RewriteCond %{REQUEST_FILENAME} !-d
  RewriteRule . /path/to/your/app/index.html [L]
</IfModule>
```

OK，这时候你可以测试一下项目的各个URL访问是否正常了。