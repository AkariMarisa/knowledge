# MySQL插入中文数据乱码
1. 首先确认操作数据库方编码是否为UTF-8，并且检查连接地址中是否配置了?useUnicode=true&characterEncoding=utf-8。
2. 在MySQL执行show variables like 'char%'，查看当前数据库编码，如果有问题，则需要修改数据库服务器上的配置文件。具体修改如下

        [mysqld]
        character-set-server=utf8mb4

        [client]
        default-character-set=utf8mb4

        [mysql]
        default-character-set=utf8mb4

    修改完成后重启MySQL即可。